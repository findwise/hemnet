# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import re
import psycopg2
from geopy.geocoders import Nominatim
from geopy.location import Location
from geopy.exc import GeopyError

def extract_number(s):
    try:
        return str(int(''.join(re.findall(r'\d+', s)))) if s else s
    except:
        try:
            return str(float(''.join(re.findall(r'\d+', s)))) if s else s
        except:
            return None

#def extract_comma_number(s):
#    return ','.join(re.findall(r'\d+', s)).replace(",", ".") if s else s
#
#def extract_int_number(s):
#    return ''.join(re.findall(r'\d+', s)) if s else s


class HousingPipeline(object):

    def process_item(self, item, spider):

        item['price'] = extract_number(item['price'][0])
        item['rooms'] = extract_number(item['rooms'][0])
        item['livingspace'] = extract_number(item['livingspace'][0])
        item['price_sqm'] = extract_number(item['price_sqm'][0])
#        item['saledate'] = item['saledate'][0].strip('\n ')
        item['askedprice'] = extract_number(item['askedprice'][0])
        item['plotarea'] = extract_number(item['plotarea'][0])
        item['addarea'] = extract_number(item['addarea'][0])
        item['operatingcost'] = extract_number(item['operatingcost'][0])
        try:
            item['buildyear'] = extract_number(item['buildyear'])[0:4]
        except (TypeError):
            item['buildyear'] = None
        item['askedprice_sqm'] = int(item['askedprice'])/int(item['livingspace']) if (item['askedprice'] and item['livingspace']) else None
        item['fee'] = extract_number(item['fee'][0])
        item['brokerfirm'] =  item['brokerfirm'].strip('\n')
        item['citypart'] = item['kommun'].split('\n')[3].strip(',').strip(' ')
        item['kommun'] = item['kommun'].split('\n')[4].strip(' ')
        
        # metadata contains a long string with info separated by "&quot"
        try:
            item['metadata'] = item['metadata'].split('&quot;')
            item['metadata'] = list(filter(lambda a: a != ';', item['metadata']))
            item['metadata'] = list(filter(lambda a: a != ',', item['metadata']))
            item['metadata'] = list(filter(lambda a: a != '', item['metadata']))
            item['metadata'] = list(filter(lambda a: a != ':', item['metadata']))
            id_index = item['metadata'].index('id') + 1
            item['id'] = int(item['metadata'][id_index].strip(':').strip(','))
            coord_index = item['metadata'].index('coordinate') + 1
            item['latitude'] = item['metadata'][coord_index].split(',')[0].strip(':').strip('[')
            item['longitude'] = item['metadata'][coord_index].split(',')[1].strip(',').strip(']')
            housetype_index = item['metadata'].index('type') + 1
            item['housetype'] = item['metadata'][housetype_index]
            sale_index = item['metadata'].index('sale_date') + 1
            item['saledate'] = item['metadata'][sale_index].strip('Såld ')
        except (AttributeError):
        # Some hemnet items do not have all the information we need. 
        # We still need to create a unique id though:
            item['id'] = int(item['price']) + int(item['price_sqm']) + int(item['askedprice'])
            item['latitude'] = None
            item['longitude'] = None
            item['housetype'] = None
            item['saledate'] = None
            
        # Example input: 'Kvartettgatan 24 - Såld bostadsrättslägenhet - Almvik, Malmö - Hemnet'
        # Some citypart names might be a combination of two areas, 
        # e.g. "Nyborg-Ytterbyn" which in turn is in the area "Kalix-Nyborg".
        # We need to take these "-" into consideration
        
        item['address'] = item['address'].strip(' - Hemnet')
        item['citypart'] = item['citypart'].strip('\n ')
        item['area'] = '-'.join(item['address'].split('Såld ')[1].split('-')[1:])
        item['address'] = item['address'].split('-')[0].strip(' ')
        
        geo = Nominatim(user_agent='john.isaksson@findwise.com', country_bias='sweden')
        coordinates = (item['latitude'], item['longitude'])
        
        try:
            location = geo.reverse(coordinates, language='SE')
        except (GeopyError):
            location = None
        
        try:
            item['region'] = location.raw['address']['state']
        except (KeyError, AttributeError):
            item['region'] = None
        
        try: 
            item['primarea'] = location.raw['address']['city_district']
        except (KeyError, AttributeError):
            item['primarea'] = None
        
        # Out of scope for scraping but required fields in database table       
        item['brokername'] = None
        item['brokeremail'] = None
        item['point_osm'] = None        
        item['kommun_id'] = None
        item['region_id'] = None        
        item['primarea_id'] = None
        item['citypart_id'] = None
        item['dist_water'] = None
        item['dist_coast'] = None

        attributes = ('id', 'latitude', 'longitude', 'address', 'housetype', 
                      'saledate', 'price', 'askedprice', 'rooms', 'livingspace', 
                      'url', 'area', 'buildyear', 'fee', 'operatingcost', 
                      'plotarea', 'addarea', 'brokername', 'brokeremail', 
                      'brokerfirm', 'point_osm', 'kommun', 'region', 'kommun_id',
                      'region_id', 'price_sqm', 'askedprice_sqm', 'primarea', 'citypart', 
                      'primarea_id', 'citypart_id', 'dist_water', 'dist_coast')
        
#        print("INSERT INTO hemnet_updated({0}) VALUES {1}".format(', '.join(attributes), tuple(item[a] for a in attributes)))
        data = tuple(item[a] for a in attributes)
#        print("INSERT INTO hemnet_updated%s VALUES %s;", (attributes, data))
        if not self.entry_exists(item['id']):
            self.cur.execute("INSERT INTO hemnet_updated VALUES %s;", (data,))
#        self.cur.execute("INSERT INTO hemnet_test({0}) VALUES {1};".format(', '.join(attributes), tuple(item[a] for a in attributes)))
            self.connection.commit()
        
        # Update the point_osm column
#        self.cur.execute("UPDATE hemnet_test2 SET point_osm = ST_Transform(ST_SetSRID(ST_MakePoint(longitude,latitude),4326),3857) where id=%s;", item['id'])
        
#        self.connection.commit()
        return item
    
    def open_spider(self, spider):
        hostname = 'localhost'
        username = 'postgres'
        password = 'findwise'
        database = 'osm'
        self.connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()
        
    def entry_exists(self, id_int):
        exists_query = "SELECT EXISTS (SELECT 1 FROM hemnet_updated WHERE id = %s);"
        self.cur.execute(exists_query, (id,))
        print("%s exists? %s", self.cur.fetchone()[0])
        return self.cur.fetchone()[0]