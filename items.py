# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class HouseItem(scrapy.Item):
    
    id = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    address = scrapy.Field()
    housetype = scrapy.Field()
    saledate = scrapy.Field()
    price = scrapy.Field()
    askedprice = scrapy.Field()
    rooms = scrapy.Field()
    livingspace = scrapy.Field()
    url = scrapy.Field()
    area = scrapy.Field()
    buildyear = scrapy.Field()
    fee = scrapy.Field()
    operatingcost = scrapy.Field()
    plotarea = scrapy.Field()
    addarea = scrapy.Field()
    brokername = scrapy.Field()
    brokeremail = scrapy.Field()
    brokerfirm = scrapy.Field()
    point_osm = scrapy.Field()
    kommun = scrapy.Field()
    region = scrapy.Field()
    kommun_id = scrapy.Field()
    region_id = scrapy.Field()
    price_sqm = scrapy.Field()
    askedprice_sqm = scrapy.Field()
    primarea = scrapy.Field()
    citypart = scrapy.Field()
    primarea_id = scrapy.Field()
    citypart_id = scrapy.Field()
    dist_water = scrapy.Field()
    dist_coast = scrapy.Field()
    metadata = scrapy.Field()
    pass
