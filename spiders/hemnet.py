# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 09:54:47 2018

@author: john.isaksson

Riktlinjerna för attribuering
När du använder Hemnets data i publicerade form, 
    vänligen ange: “Källa: Hemnet”.
    
När du använder Hemnets analyser eller citat, vänligen ange: 
    “Staffan Tell, talesperson på Hemnet”.
    
Upphovsrätt för objektsbilder på Hemnet tillhör respektive mäklare 
    eller fotograf. Hemnet har inte möjlighet att godkänna användande av bilder.
"""

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from hemnet.items import HouseItem
import logging

class HemnetSpider(CrawlSpider):
    name = 'hemnet' 
    # Sold the past month
    # start_urls = ['https://www.hemnet.se/salda/bostader?page=202&sold_age=1m']
    
    # Example object
    #start_urls = ['https://www.hemnet.se/salda/bostader?page=1321&sold_age=1m']
    
    # Sold all time
    #start_urls = ['https://www.hemnet.se/salda/bostader?page=1&sold_age=all']
    allowed_domains = ['hemnet.se']
    
    rules = [
        # Links to sold items
        Rule(LinkExtractor(restrict_xpaths = "//a[@class='item-link-container']"), callback='parse_object'),
        # Link to next page
        Rule(LinkExtractor(restrict_xpaths = "//a[@class='next_page button button--primary']"), follow=True)
    ]

    
#    def parse(self, response):      
#        # Follow links to objects
#        for object_page in response.css('a.item-link-container::attr(href)').extract():
#            if object_page is not None:
#                yield response.follow(object_page, self.parse_object)
#                
#        # Follow pagination links
#        next_page = response.css('a.next_page::attr(href)').extract_first()
#        print('Next page: ', next_page)
#        if next_page is not None:
#            next_page = response.urljoin(next_page)
#            print('Next page: ', next_page)
#            yield scrapy.Request(object_page, callback=self.parse)
        
    def parse_object(self, response):
        item = HouseItem()
        item['address'] = response.css("title::text").extract_first()
#        item['housetype'] = response.css("span.svg-icon__fallback-text::text").extract_first(),               
        item['price'] = response.css("span.sold-property__price-value::text").extract_first(),            
        item['saledate'] = response.css("time::text").extract_first(),
        item['price_sqm'] = response.xpath("//dt[text() = 'Pris per kvadratmeter']/following-sibling::dd[1]/text()").extract_first(),
        item['askedprice'] = response.xpath("//dt[text() = 'Begärt pris']/following-sibling::dd[1]/text()").extract_first(),               
        item['rooms'] = response.xpath("//dt[text() = 'Antal rum']/following-sibling::dd[1]/text()").extract_first(), 
        item['livingspace'] = response.xpath("//dt[text() = 'Boarea']/following-sibling::dd[1]/text()").extract_first(), 
        item['fee'] = response.xpath("//dt[text() = 'Avgift/månad']/following-sibling::dd[1]/text()").extract_first(), 
        item['buildyear'] = response.xpath("//dt[text() = 'Byggår']/following-sibling::dd[1]/text()").extract_first()
        item['operatingcost'] = response.xpath("//dt[text() = 'Driftskostnad']/following-sibling::dd[1]/text()").extract_first(), 
        item['url'] = response.request.url
        item['brokerfirm'] =  response.css("div.broker__details a::text").extract_first()         
        item['plotarea'] = response.xpath("//dt[text() = 'Tomtarea']/following-sibling::dd[1]/text()").extract_first(), 
        item['addarea'] = response.xpath("//dt[text() = 'Biarea']/following-sibling::dd[1]/text()").extract_first(), 
        item['metadata'] = response.css("div.js-listing-map-sold").extract_first()
        item['kommun'] = response.css('p.sold-property__metadata::text').extract()[1]
        # item['forening'] = response.xpath("//dt[text() = 'Förening']/following-sibling::dd[1]/text()").extract_first()
 
        # Handled in pipeline
#        item['area']
        item['citypart'] = response.css('span.area::text').extract_first()
#        item['askedprice_sqm']
#        item['id']
#        item['housetype']
#        item['latitude']
#        item['longitude']
        
        # Out of scope for scraping but required fields in database table       
#        item['brokername']
#        item['brokeremail']
#        item['point_osm']
#        item['kommun']
#        item['region']
#        item['kommun_id']
#        item['region_id']        
#        item['primarea']
#        item['primarea_id']
#        item['citypart_id']
#        item['dist_water']
#        item['dist_coast']
       
        yield item